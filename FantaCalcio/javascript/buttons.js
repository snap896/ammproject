/**
 * @author Manuele Flore
 */

function logout(role){
    if(confirm('Sei sicuro di volerti disconnettere?') === true)
        location.href = role + '?cmd=logout';
}