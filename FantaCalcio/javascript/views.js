/**
 * Script in javascript per la modifica dinamica delle viste
 * 
 * @author: Manuele Flore
 */

$(document).ready(function(){
    
    
    //nasconde l'anagrafica dei giocatori
    $('#player').hide();
    
    /** 
     * variabili utili per lo scorrimento dei div laterali quando si scorre la pagina
     * @var ele tutti i div "scrollabili"
     * @var coordinate_top la distanza dei div e del campo da calcio della formazione 
     * dalla cima del documento (#main_container)
     */
    var ele = $('.scrollable');
    var coordinate_top = ele.offset().top;
    
    /*
     * modifica dell'aspetto campo calcio in base alle scelte del modulo
     */
    if($('#campo_calcio').length){
        //nasconde il portiere (unico elemento che non cambia mai posizione)
        $('#portiere').hide();
        //coordinate per lo scroll del div
        var coordinate_campo = $('#campo_calcio').offset().top;
        /*
         * la funzione si attiva con il cambio dei valori della select
         * nella vista /formazione
         * serve a impostare i div che conterranno i titolari della formazione scelta dall'utente
         */
        $('#modulo').change(function(){
            //globale, serve anche al submit della formazione in formazione.js
            _modulo = {
                 'defense' : parseInt($(this).val().substr(0,1)),
                 'midfield' : parseInt($(this).val().substr(2,1)),
                 'forward' : parseInt($(this).val().substr(4,1))
            };
            
            $('#portiere').show();
            //svuota la vista
            $('.strips').has('.titolari').empty();
            //riempe la vista in base al modulo
            for(var i = 0, j = -1; i < _modulo['defense']; i++){
                $('#difesa').append("<div id=\"" + j + "\" class=\"titolari\" ondrop=\"drop(event)\" ondragover=\"dragOver(event)\"\n\
                                        ></div>");
                j--;
            }
            for(var i = 0; i < _modulo['midfield']; i++){
                $('#centrocampo').append("<div id=\"" + j + "\" class=\"titolari\" ondrop=\"drop(event)\" ondragover=\"dragOver(event)\"\n\
                                        ></div>");
                j--;
            }
            for(var i = 0; i < _modulo['forward']; i++){
                $('#attacco').append("<div id=\"" + j + "\" class=\"titolari\" ondrop=\"drop(event)\" ondragover=\"dragOver(event)\"\n\
                                        ></div>");
                j--;
            }
            
            switch(_modulo['defense']){
                case 3:
                    $('#difesa .titolari').css("marginRight", "70px");
                    $('#difesa').css("paddingLeft", "105px");
                    break;
                case 4:
                    $('#difesa .titolari').css("marginRight", "65px");
                    $('#difesa').css("paddingLeft", "50px");
                    break;
                case 5:
                    $('#difesa .titolari').css("marginRight", "47px");
                    $('#difesa').css("paddingLeft", "25px");
                    break;
                default:
                    break;
            }
            switch(_modulo['midfield']){
                case 3:
                    $('#centrocampo .titolari').css("marginRight", "70px");
                    $('#centrocampo').css("paddingLeft", "105px");
                    break;
                case 4:
                    $('#centrocampo .titolari').css("marginRight", "65px");
                    $('#centrocampo').css("paddingLeft", "50px");
                    break;
                case 5:
                    $('#centrocampo .titolari').css("marginRight", "47px");
                    $('#centrocampo').css("paddingLeft", "25px");
                    break;
                default:
                    break;
            }
            switch(_modulo['forward']){
                case 3:
                    $('#attacco .titolari').css("marginRight", "70px");
                    $('#attacco').css("paddingLeft", "105px");
                    break;
                case 2:
                    $('#attacco .titolari').css("marginRight", "65px");
                    $('#attacco').css("paddingLeft", "170px");
                    break;
                case 1:
                    $('#attacco').css("paddingLeft", "233px");
                    break;
                default:
                    break;
            }
        });  
    }
    /*
     * funzione che fa scorrere i div laterali impostando l'attributo top degli elementi dinamicamente allo
     * scorrere della scrollbar della pagina
     */
    $(window).scroll(function(){
        //coordinate della scrollbar
        var scroll_top = $(this).scrollTop();
        if(scroll_top < coordinate_top)
            ele.stop().animate({'top': 0}, 200);
        else
            ele.stop().animate({'top': scroll_top - coordinate_top + 10}, 200);
        if($('#campo_calcio').length){
            if(scroll_top < coordinate_campo)
                $('#campo_calcio').stop().animate({'top': 0}, 200);
            else
                $('#campo_calcio').stop().animate({'top': scroll_top - coordinate_campo + 10}, 200);
        }
    });
    
    /*
     * imposta l'altezza dei div laterali in base all'altezza del content
     */
    $(window).load(function(){
        var height = $("#content").outerHeight(true);
        $("#navigation").height(height);  
        $("#side2").height(height);
    });
    
    /*------------------------->>AJAX<<----------------------------------*/
    
    /*
     * mostra l'anagrafica dei giocatori quando si passa sopra il loro nome
     */
    $('.info_player').hover(function(){
        $('#player').show();
        //raccoglie l'id degli elementi .info_player
        var idPlayer = this.id;
        $.ajax({
           url: "user/giocatori",
           data: {giocatore : idPlayer},
           dataType: "json",
           success: function(data, state){
               $('#nomePlayer').text(data['nome'] + " " + data['cognome']);
               $('#ruolo').text(data['ruolo']);
               $('#squadra').attr('src', data['url'] + '/img/loghiSquadre/' + data['squadra']);
               $('#naz').attr('src', data['url'] + '/img/loghiNaz/' + data['naz']);
               $('#pres').text(data['pres']);
               $('#gol').text(data['gol']);
               $('#prezzo').text(data['prezzo']);
               $('#foto_p').attr('src', data['url'] + '/img/fotoPlayer/' + data['foto']);
           },
           error: function(data, state){
               $('#player').text("Errore: Connessione fallita.\nImpossibile caricare info giocatori");
           }
        });
    });
}); 