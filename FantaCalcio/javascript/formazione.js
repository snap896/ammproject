/* 
 * script per la gestione e il submit della formazione
 * 
 * @author Manuele Flore
 */

function dragStart(event){
    event.dataTransfer.setData('text', event.target.id);
    $.ajax({
       url: 'user/player',
       data: {player : event.target.id},
       dataType: 'json',
       success: function(data, state){
            player = {
                id : data['id'],
                ruolo : data['ruolo']
            };
       },
       error: function(data, state){
           
       }
    });
}

function dragEnter(event){
    
}

function dragOver(event){
    if(event.preventDefault())
        event.preventDefault();
    return false;
}



function drop(event){
    if(event.stopPropagation())
        event.stopPropagation();
    if(event.preventDefault())
        event.preventDefault();
    event.target.style.opacity = '1';
    var id = event.dataTransfer.getData('text');
    event.target.appendChild(document.getElementById(id));
    return false;
}