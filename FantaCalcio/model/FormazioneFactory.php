<?php

/**
 * Factory di Formazione, contiene tutti i metodi per creare degli oggetti 
 * formazione in base ai dati del database e viceversa
 * 
 * @author Manuele Flore
 */

class FormazioneFactory{
    
    /**
     * variabile che contiene la singola istanza della classe
     * @var FormazioneFactory
     */
    private static $singleton;
    
    private function __construct() {}
    
    public static function getInstance(){
        if(!isset(self::$singleton))
            self::$singleton = new FormazioneFactory();
        return self::$singleton;
    }
    
    private function serializeIdPlayer($idPlayer){
        foreach($idPlayer as $id)
            $result.= $id.'-';
        return $result;
    }
    
    private function explodeIdPlayer($idPlayer){
        $id = explode('-', $idPlayer);
        return $id;
    }
}