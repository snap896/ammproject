<?php

/**
 * 
 * @author Manuele Flore
 */

class Squadra{
    
    private $id;
    
    private $nome;
    
    private $crediti;
    
    private $logo;
    
    
    public function __construct($id, $nome, $crediti, $logo){
        $this->id = $id;
        $this->nome = $nome;
        $this->crediti = $crediti;
        $this->logo = $logo;
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getNome(){
        return $this->nome;
    }
    
    public function getCrediti(){
        return $this->crediti;
    }
    
    public function getLogo(){
        return $this->logo;
    }    
}
