<?php

include_once 'Player.php';
include_once 'Database.php';

/**
 * Description of PlayerFactory
 *
 * @author Manuele Flore
 */

class PlayerFactory {
    
    private static $singleton;
    
    private function __construct(){}
    
    /**
     * costruttore delegato della factory, fa si che venga creata una sola
     * istanza di PlayerFactory
     * 
     * @return PlayerFactory
     */
    public static function getInstance(){
        if(!isset(self::$singleton))
            self::$singleton = new PlayerFactory();
        return self::$singleton;
    }
    
    public function getListaPlayer($role){
        $list = array();
        $mysqli = Database::getInstance()->connect_db();
        if(!$mysqli){
            error_log("lista giocatori: errore nella connessione al database");
            return $list;
        }
        $query = "SELECT * FROM ListaGiocatori WHERE ruolo LIKE ?;";
        $stmt = $mysqli->stmt_init();
        $stmt->prepare($query);
        if(!$stmt){
            error_log("ListaGiocatori(getListaPlayer): prepare fallita");
            $stmt->close();
            $mysqli->close();
            return $list;
        }
        if(!$stmt->bind_param('s', $role)){
            error_log("ListaGiocatori(getListaPlayer): bind fallita");
            $stmt->close();
            $mysqli->close();
            return $list;
        }
        if(!$stmt->execute()){
            error_log("ListaGiocatori(getListaPlayer): execute fallita");
            $stmt->close();
            $mysqli->close();
            return $list;
        }
        if(!$stmt->bind_result($res_id, $res_nome, $res_cognome, $res_squadra, $res_ruolo, $res_naz,
                $res_pres, $res_gol, $res_prezzo, $res_foto)){
            error_log("ListaGiocatori(getListaPlayer): bind_result fallita");
            $stmt->close();
            $mysqli->close();
            return $list;
        }
        while($stmt->fetch())
            $list[] = new Player($res_id, $res_nome, $res_cognome, $res_squadra, $res_ruolo, $res_naz,
                    $res_pres, $res_gol, $res_prezzo, $res_foto);
        $stmt->close();
        $mysqli->close();
        return $list;
    }
    
    public function getPlayerbyId($id){
        $player = null;
        $mysqli = Database::getInstance()->connect_db();
        if(!$mysqli){
            error_log("ListaGiocatori(getPlayer): connessione databasefallita ");
            $mysqli->close();
            return null;
        }
        $query = "SELECT * FROM ListaGiocatori WHERE id = ?;";
        $stmt = $mysqli->stmt_init();
        $stmt->prepare($query);
        if(!$stmt){
            error_log("ListaGiocatori(getPlayer): prepare fallita");
            $stmt->close();
            $mysqli->close();
            return null;
        }
        if(!$stmt->bind_param('s', $id)){
            error_log("ListaGiocatori(getPlayer): bind fallita");
            $stmt->close();
            $mysqli->close();
            return null;
        }
        if(!$stmt->execute()){
            error_log("ListaGiocatori(getPlayer): execute fallita");
            $stmt->close();
            $mysqli->close();
            return null;
        }
        if(!$stmt->bind_result($res_id, $res_nome, $res_cognome, $res_squadra, $res_ruolo, $res_naz,
                $res_pres, $res_gol, $res_prezzo, $res_foto)){
            error_log("ListaGiocatori(getPlayer): bind_result fallita");
            $stmt->close();
            $mysqli->close();
            return null;
        }
        while($stmt->fetch())
            $player = new Player($res_id, $res_nome, $res_cognome, $res_squadra, $res_ruolo, $res_naz,
                    $res_pres, $res_gol, $res_prezzo, $res_foto);
        $stmt->close();
        $mysqli->close();
        return $player;
    }
    
    public function getPlayerbyTeamId($id){
        $lista = array();
        $mysqli = Database::getInstance()->connect_db();
        if(!$mysqli){
            error_log("PlayerFactory(getPlayerbyTeamId): fallita connessione al db");
            $mysqli->close();
            return null;
        }
        $result = $mysqli->query("SELECT * FROM ListaGiocatori "
                . "JOIN giocatori_squadre ON ListaGiocatori.id = giocatori_id "
                . "WHERE squadre_id = $id;");
        if($mysqli->errno > 0){
            error_log("PlayerFactory(getPlayerbyTeamId): query error: $mysqli->errno, $mysqli->error");
            $mysqli->close();
            return null;
        }
        while($row = $result->fetch_object()){
            $lista[] = new Player($row->id, $row->nome, $row->cognome, $row->squadra, 
                    $row->ruolo, $row->naz, $row->presenze, $row->gol, $row->prezzo, $row->foto);
        }
        $mysqli->close();
        return $lista;
    }
    
    /*----------------------->>>TRANSAZIONI<<<------------------------*/
    
    public function Compra($id_player, $id_squadra, $prezzo, $crediti){
        $msg = null;
        $mysqli = Database::getInstance()->connect_db();
        if(!mysqli){
            error_log("PlayerFactory(Compra): fallita connessione al db");
            $mysqli->close();
            return $msg;
        }
        $mysqli->autocommit(false);
        $mysqli->query("INSERT INTO giocatori_squadre(giocatori_id, squadre_id) VALUES($id_player, $id_squadra);");
        if($mysqli->errno > 0){
            error_log("PlayerFactory(Compra): query error: $mysqli->errno, $mysqli->error");
            $mysqli->rollback();
            $mysqli->close();
            return $msg;
        }
        if($prezzo > $crediti){
            $msg = "Non hai i crediti sufficenti per acquistare il giocatore";
            $mysqli->rollback();
            $mysqli->close();
            return $msg;
        }
        $crediti -= $prezzo;
        $mysqli->query("UPDATE Squadre SET crediti = $crediti WHERE id = $id_squadra");
        if($mysqli->errno > 0){
            error_log("PlayerFactory(Compra): query error: $mysqli->errno, $mysqli->error");
            $mysqli->rollback();
            $mysqli->close();
            return $msg;
        }
        $mysqli->commit();
        $mysqli->autocommit(true);
        $mysqli->close();
        $msg = 'acquistato!';
        return $msg;
    }
    
    public function Vendi($id_player, $id_squadra, $prezzo, $crediti){
        $msg = null;
        $mysqli = Database::getInstance()->connect_db();
        if(!mysqli){
            error_log("PlayerFactory(Vendi): fallita connessione al db");
            $mysqli->close();
            return $msg;
        }
        $mysqli->query("DELETE FROM giocatori_squadre WHERE giocatori_id = $id_player AND squadre_id = $id_squadra;");
        if($mysqli->errno > 0){
            error_log("PlayerFactory(Vendi): query error: $mysqli->errno, $mysqli->error");
            $mysqli->close();
            return $msg;
        }
        $crediti += $prezzo;
        $mysqli->query("UPDATE Squadre SET crediti = $crediti WHERE id = $id_squadra");
        if($mysqli->errno > 0){
            error_log("PlayerFactory(Vendi): query error: $mysqli->errno, $mysqli->error");
            $mysqli->close();
            return $msg;
        }
        $mysqli->close();
        $msg = 'acquistato!';
        return $msg;
    }
}