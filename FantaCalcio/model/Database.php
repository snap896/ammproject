<?php

include_once 'Settings.php';

/**
 * Classe che rappresenta il database nel modello, stabilisce una connessione con il db.
 * @author Manuele Flore
 */

class Database {
    
    private static $singleton;
    /**
     * costruttore della classe Database: private
     * la classe database viene istanziata tramite il metodo getInstance()
     */
    private function __construct() {}
    
    /**
     * costruttore reale della classe Database, permette di istanziare un solo
     * oggetto di tipo Database
     * @return Database
     */
    public static function getInstance(){
        if(!isset(self::$singleton)){
            self::$singleton = new Database();
        }
        return self::$singleton;
    }
    
    /**
     * restituisce una connessione al database.
     * @return mysqli
     */
    public function connect_db(){
        $mysqli = new mysqli();
        $mysqli->connect(Settings::DB_HOST, Settings::DB_USER,
                Settings::DB_PASS, Settings::DB_NAME);
        if($mysqli->connect_errno != 0){
            $id_error = $mysqli->connect_errno;
            $message = $mysqli->connect_error;
            error_log("Errore nella connessione al database, id: $id_error, $message", 0);
            return null;
        }
        return $mysqli;
    }   
}