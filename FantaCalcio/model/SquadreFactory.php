<?php

include_once 'Squadra.php';

/**
 * @author Manuele Flore
 */

class SquadreFactory{
    
    private static $singleton;
    
    private function __construct() {}
    
    public static function getInstance(){
        if(!isset(self::$singleton))
            self::$singleton = new SquadreFactory();
        return self::$singleton;
    }
    
    public function getlistaSquadre($user){
        $lista = array();
        $mysqli = Database::getInstance()->connect_db();
        if(!$mysqli){
            error_log("Squadre(getlistaSquadre): fallita connessione al database");
            $mysqli->close();
            return $lista;
        }
        $query = "SELECT * FROM Squadre WHERE possiede_username = ?;";
        $stmt = $mysqli->stmt_init();
        $stmt->prepare($query);
        if(!$stmt){
            error_log("Squadre(getlistaSquadre): impossibile inizializzare lo statement");
            $stmt->close();
            $mysqli->close();
            return $lista;
        }
        if(!$stmt->bind_param('s', $user)){
            error_log("Squadre(getlistaSquadre): bind_param fallito");
            $stmt->close();
            $mysqli->close();
            return $lista;
        }
        if(!$stmt->execute()){
            error_log("Squadre(getlistaSquadre): esecuzione query fallita");
            $stmt->close();
            $mysqli->close();
            return $lista;
        }
        if(!$stmt->bind_result($id, $nome, $crediti, $logo, $fkey)){
            error_log("Squadre(getlistaSquadre): bind_result fallita");
            $stmt->close();
            $mysqli->close();
            return $lista;
        }
        while($stmt->fetch())
            $lista[] = new Squadra($id, $nome, $crediti, $logo);
        $stmt->close();
        $mysqli->close();
        return $lista;
    }
    
    public function getSquadra($name){
        $team = null;
        $mysqli = Database::getInstance()->connect_db();
        if(!$mysqli){
            error_log("Squadre(getSquadra): connessione databasefallita ");
            $mysqli->close();
            return null;
        }
        $query = "SELECT * FROM Squadre WHERE nome = ?";
        $stmt = $mysqli->stmt_init();
        $stmt->prepare($query);
        if(!$stmt){
            error_log("Squadre(getSquadra): prepare fallita");
            $stmt->close();
            $mysqli->close();
            return null;
        }
        if(!$stmt->bind_param('s', $name)){
            error_log("Squadre(getSquadra): bind fallita");
            $stmt->close();
            $mysqli->close();
            return null;
        }
        if(!$stmt->execute()){
            error_log("Squadre(getSquadra): execute fallita");
            $stmt->close();
            $mysqli->close();
            return null;
        }
        if(!$stmt->bind_result($res_id, $res_nome, $res_crediti, $res_logo, $fkey)){
            error_log("Squadre(getSquadra): bind_result fallita");
            $stmt->close();
            $mysqli->close();
            return null;
        }
        while($stmt->fetch())
            $team = new Squadra($res_id, $res_nome, $res_crediti, $res_logo);
        $stmt->close();
        $mysqli->close();
        return $team;
    }
    
    public function getSquadrabyId($id){
        $team = null;
        $mysqli = Database::getInstance()->connect_db();
        if(!$mysqli){
            error_log("Squadre(getSquadrabyId): connessione database fallita");
            $mysqli->close();
            return $team;
        }
        $query = "SELECT * FROM Squadre WHERE id = $id";
        $result = $mysqli->query($query);
        if($mysqli->errno > 0){
            error_log("Squadre(getSquadrabyId): query error: $mysqli->errno, $mysqli->error");
            $mysqli->close();
            return $team;
        }
        while($row = $result->fetch_object())
                $team = new Squadra ($row->id, $row->nome, $row->crediti, $row->logo);
        $mysqli->close();
        return $team;
    }
    
    public function crea($nome, $logo, $user){
        $mysqli = Database::getInstance()->connect_db();
        if(!$mysqli){
            error_log("Squadre(crea): fallita connessione");
            $mysqli->close();
            return false;
        }
        $query = "INSERT INTO Squadre(nome, crediti, logo, possiede_username) VALUES (?, default, ?, ?);";
        $stmt = $mysqli->stmt_init();
        $stmt->prepare($query);
        if(!$stmt){
            error_log("Squadre(crea): prepare fallita");
            $stmt->close();
            $mysqli->close();
            return false;
        }
        if(!$stmt->bind_param('sss', $nome, $logo, $user)){
            error_log("Squadre(crea): bind_param fallita");
            $stmt->close();
            $mysqli->close();
            return false;
        }
        if(!$stmt->execute()){
            error_log("Squadre(crea): execute fallita");
            $stmt->close();
            $mysqli->close();
            return false;
        }
        $stmt->close();
        $mysqli->close();
        return true;
    }
}