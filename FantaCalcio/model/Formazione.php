<?php

/**
 * Classe del modello, rappresenta le formazioni
 * @author Manuele Flore
 */
class Formazione{
    
    /**
     * id della formazione
     * @var int
     */
    private $id;
    
    /**
     * variabile che contiene il modulo di gioco
     * @var string
     */
    private $modulo;
    
    /**
     * lista degli id dei giocatori schierati
     * @var string[]
     */
    private $idGiocatori;
    
    /**
     * costruttore, inizializza gli attributi della classe Formazione
     * @param int $id
     * @param string $modulo
     * @param string[] $idGiocatori
     */
    public function __construct($id, $modulo, $idGiocatori) {
        $this->id = $id;
        $this->modulo = $modulo;
        $this->idGiocatori = $idGiocatori;
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getModulo(){
        return $this->modulo;
    }
    
    public function get_idGiocatori(){
        return $this->idGiocatori;
    }
}

