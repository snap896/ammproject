<?php

/** 
 * classe che rappresenta il singolo calciatore
 * 
 * @author Manuele Flore
 */

class Player{
    
    /**
     * id del calciatore
     * @var int
     */
    private $id;
    
    /**
     * nome del calciatore
     * @var string
     */
    private $nome;
    
    /**
     * cognome del calciatore
     * @var string
     */
    private $cognome;
    
    /**
     * squadra del calciatore
     * @var string
     */
    private $squadra;
    
    /**
     * ruolo del calciatore(P = portiere, D = difensore, 
     * C = centrocampista, A = attaccante)
     * @var char
     */
    private $ruolo;
    
    /**
     * nazionalità del calciatore (sigla ISO 3166-1 alpha-3)
     * @var string
     */
    private $naz;
    
    /**
     * presenze del calciatore in serie A
     * @var int
     */
    private $presenze;
    
    /**
     * numero di gol segnati/subiti
     * @var int
     */
    private $gol;
    
    /**
     * prezzo del calciatore in fantacrediti
     * @var int
     */
    private $prezzo;
    
    /**
     * foto del calciatore
     * @var string
     */
    private $foto;
    
    /**
     * costruttore, crea i singoli oggetti player e imposta i suoi attributi
     * @param int AUTO $id
     * @param string $nome
     * @param string $cognome
     * @param string $squadra
     * @param char $ruolo
     * @param string $naz
     * @param int $presenze
     * @param int $gol
     * @param int $prezzo
     * @param string $foto
     */
    public function __construct($id, $nome, $cognome, $squadra, $ruolo, $naz, $presenze, $gol, $prezzo, $foto){
        $this->id = $id;
        $this->nome = $nome;
        $this->cognome = $cognome;
        $this->squadra = $squadra;
        $this->ruolo = $ruolo;
        $this->naz = $naz;
        $this->presenze = $presenze;
        $this->gol = $gol;
        $this->prezzo = $prezzo; 
        $this->foto = $foto;
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getNome(){
        return $this->nome;
    }
    
    public function getCognome(){
        return $this->cognome;
    }

    public function getRuolo(){
        return $this->ruolo;
    }

    public function getNaz(){
        return $this->naz;
    }
    
    
    public function getSquadra(){
        return $this->squadra;
    }
      
    public function getPres(){
        return $this->presenze;
    }
    
    public function getGol(){
        return $this->gol;
    }
    
    public function getPrezzo(){
        return $this->prezzo;
    }
    
    public function getFoto(){
        return $this->foto;
    }
}