<?php

include_once 'Database.php';
include_once 'User.php';

/** 
 * @author Manuele Flore
 */

class UserFactory{
    
    private static $singleton;
    
    private function __construct(){}

    public static function getInstance(){
        if(!isset(self::$singleton))
            self::$singleton = new UserFactory();
        return self::$singleton;        
    }
    
    public function autentica($user, $pass){
        $utente = null;
        $mysqli = Database::getInstance()->connect_db();
        if(!isset($mysqli)){
            error_log("Users(autentica): errore nella connessione al database");
            return null;
        }
        $query = "SELECT * FROM Users WHERE (Users.username = ? OR Users.mail = ?) AND Users.password = ?;";
        $stmt = $mysqli->stmt_init();
        $stmt->prepare($query);
        if(!$stmt){
            error_log("Users(autentica): impossibile inizializzare lo statement");
            $mysqli->close();
            $stmt->close();
            return null;
        }
        if(!$stmt->bind_param('sss', $user, $user, $pass)){
            error_log("Users(autentica): bind_param fallito");
            $stmt->close();
            $mysqli->close();
            return null;
        }
        if(!$stmt->execute()){
            error_log("Users(autentica): impossibile eseguire lo statement");
            $stmt->close();
            $mysqli->close();
            return null;
        }
        if(!$stmt->bind_result($r_user, $r_pass, $r_mail, $r_punt, $r_adm, $r_img)){
            error_log("Users(autentica): bind_result fallito");
            $stmt->close();
            $mysqli->close();
            return null;
        }
        while($stmt->fetch())
            $utente = new User($r_user, $r_pass, $r_mail, $r_punt, $r_adm, $r_img);
        $stmt->close();
        $mysqli->close();
        return $utente;
    }
    
    public function cercaUtente($user){
        $utente = null;
        $mysqli = Database::getInstance()->connect_db();
        if(!isset($mysqli)){
            error_log("Users(cercaUtente): connessione al database fallita");
            $mysqli->close();
            return null;
        }
        $query = "SELECT * FROM Users WHERE Users.username = ?;";
        $stmt = $mysqli->stmt_init();
        $stmt->prepare($query);
        if(!$stmt){
            error_log("Users(cercaUtente): impossibile inizializzare lo statement");
            $stmt->close();
            $mysqli->close();
            return null;
        }
        if(!$stmt->bind_param('s', $user)){
            error_log("Users(cercaUtente): bind_param fallito");
            $stmt->close();
            $mysqli->close();
            return null;
        }
        if(!$stmt->execute()){
            error_log("Users(cercaUtente): impossibile eseguire lo statement");
            $stmt->close();
            $mysqli->close();
            return null;
        }
        if(!$stmt->bind_result($r_user, $r_pass, $r_mail, $r_punt, $r_adm, $r_img)){
            error_log("Users(cercaUtente): bind_result fallito");
            $stmt->close();
            $mysqli->close();
            return null;
        }
        while($stmt->fetch())
            $utente = new User($r_user, $r_pass, $r_mail, $r_punt, $r_adm, $r_img);
        $stmt->close();
        $mysqli->close();
        return $utente;
    }
    
    public function crea($user, $pass, $mail, $image){
        $mysqli = Database::getInstance()->connect_db();
        if(!isset($mysqli)){
            error_log("Users(addUser): connessione al database fallita");
            $mysqli->close();
            return false;
        }
        $query = "INSERT INTO Users (username, password, mail, punteggio, admin, imgSource) VALUES (?, ?, ?, default, 0, ?);";
        $stmt = $mysqli->stmt_init();
        $stmt->prepare($query);
        if(!$stmt){
            error_log("Users(addUser): impossibile inizializzare lo statement");
            $stmt->close();
            $mysqli->close();
            return false;
        }
        if(!$stmt->bind_param('ssss', $user, $pass, $mail, $image)){
            error_log("Users(addUser): bind_param fallito");
            $stmt->close();
            $mysqli->close();
            return false;
        }
        if(!$stmt->execute()){
            error_log("Users(addUser): impossibile eseguire lo statement");
            $stmt->close();
            $mysqli->close();
            return false;
        }
        $mysqli->close();
        $stmt->close();
        return true;
    }
}