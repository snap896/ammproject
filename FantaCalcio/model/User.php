<?php

/**
 * 
 * @author Manuele Flore
 */

class User{
    
    
    private $username;
    
    
    private $password;
    
    
    private $mail;
    
    
    private $punteggio;
    
    
    private $admin;
    
    
    private $image;
    
    
    public function __construct($username, $password, $mail, $punteggio, $admin, $image) {
        $this->username = $username;
        $this->password = $password;
        $this->mail = $mail;
        $this->punteggio = $punteggio;
        $this->admin = $admin;
        $this->image = $image;
    }
    
    public function getUser(){
        return $this->username;
    }
    
    public function getPass(){
        return $this->password;
    }
    
    public function getMail(){
        return $this->mail;
    }
    
    public function getPunteggio(){
        return $this->punteggio;
    }
    
    public function getAdmin(){
        return $this->admin;
    }
    
    public function getImage(){
        return $this->image;
    }
}