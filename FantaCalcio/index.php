<?php

include_once 'controller/defaultController.php';
include_once 'controller/adminController.php';
include_once 'controller/userController.php';

date_default_timezone_set("Europe/Rome");

index::dispatch($_REQUEST);

class index{
    
    public static function dispatch(&$request){
        session_start();
        if(isset($request['page'])){
            switch($request['page']){
                case 'home':
                    $controller = new defaultController();
                    $controller->pageSet($request);
                    break;
                case 'user':
                    if(isset($_SESSION['role']) && $_SESSION['role'])
                        self::forbidden403();
                    $controller = new userController();
                    $controller->pageSet($request);
                    break;
                case 'admin':
                    if(isset($_SESSION['role']) && !$_SESSION['role'])
                        self::forbidden403();
                    $controller = new userController();
                    $controller->pageSet($request);
                    break;
                default:
                    self::notfound404();
                    break;
            }
        }
        else{
            self::notfound404();
        }
    }
    
    public static function notfound404(){
        header("HTTP/1.0 404 NOT FOUND");
        include_once 'view404.php';
        exit();
    }
    
    public static function forbidden403(){
        header("HTTP/1.0 403 FORBIDDEN");
        include_once 'view403.php';
        exit();
    }
}