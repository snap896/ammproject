<?php

include_once 'model/PlayerFactory.php';
include_once 'model/SquadreFactory.php';

class userController extends defaultController{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function pageSet(&$request){
        $vd = new ViewDescriptor();
        if($this->logged($vd)){
            $utente = $this->getUtente();
            $listaSquadre = $this->creaListaSquadre($utente);
            if(isset($request['subpage'])){
                switch($request['subpage']){
                    case 'listaGiocatori':
                        $listaPlayer = $this->creaListaPlayer('%');
                        $vd->setSubcontent('listaGiocatori');
                        break;
                    case 'news':
                        $vd->setSubcontent('news');
                        break;
                    case 'squadre':
                        $vd->setSubcontent('menuSquadre');
                        break;
                    case 'nuova_squadra':
                        if(count($listaSquadre) < 3)
                            $vd->setSubcontent('creaSquadra');
                        else 
                            $vd->setSubcontent('menuSquadre');
                        break;
                    case $this->getSquadra($team, $request['subpage']):
                        $_SESSION['idteam'] = $team->getId();
                        $_SESSION['nometeam'] = $team->getNome();
                        $_SESSION['crediti'] = $team->getCrediti();
                        $listaTeam = $this->getGiocatoriTeam($team->getId());
                        $vd->setSubcontent('squadra');
                        break;
                    case 'mercato':
                        if(isset($request['role'])){
                            $listaPlayer = $this->creaListaPlayer($request['role']);
                            $listaTeam = $this->getGiocatoriTeam($_SESSION['idteam']);
                            $listaCompra = array_udiff($listaPlayer, $listaTeam, 
                                                        function($arr1, $arr2){
                                                            return $arr1->getId() - $arr2->getId();
                                                        });
                            $vd->setSubcontent('mercato');
                        }
                        break;
                    case 'giocatori':
                        $vd->setJson();
                        $player = $this->getGiocatore($request['giocatore']);
                        include_once 'view/utente/giocatori.php';
                        break;
                    case 'formazione':
                        $vd->setSubcontent('formazione');
                        $lista = $this->getGiocatoriTeam($_SESSION['idteam']);
                        break;
                    case 'player':
                        $vd->setJson();
                        $player = $this->getGiocatore($request['player']);
                        include_once 'view/utente/player_json.php';
                    default:
                        break;
                }
            }

            if(isset($request['cmd'])){
                switch($request['cmd']){
                    case 'logout':
                        $this->logout();
                        $this->showLogin($vd);
                        break;
                    case 'creaSquadra':
                        $reg_nome = isset($request['reg_nomeSquadra']) ? $request['reg_nomeSquadra'] : '';
                        $reg_logo = $this->uploadImage('reg_imageSquadra', $reg_nome, 'loghiUser');
                        if(isset($reg_logo))
                            $this->creaSquadra($vd, $reg_nome, $reg_logo, $utente->getUser());
                        else
                            $this->creaSquadra($vd, $reg_nome, NULL, $utente->getUser());
                        break;
                    case 'compra':
                        $msg = $this->compraGiocatore($request['player']);
                        if(isset($msg))
                            print($msg);
                        else 
                            print('non comprato :(');
                        header('location: '.Settings::getHost().'/user/'.$team->getNome());
                        break;
                    case 'vendi':
                        $msg = $this->vendiGiocatore($request['id_player']);
                        if(isset($msg))
                            print($msg);
                        else 
                            print('errore');
                        header('location: '.Settings::getHost().'/user/'.$team->getNome());
                        break;
                    default:
                        break;
                }
            }
            else 
                $this->showUtente($vd);
        }
        require 'view/master.php';
    } 
    
    private function creaSquadra($vd, $nome, $logo, $user){
        if(!SquadreFactory::getInstance()->crea($nome, $logo, $user))
            echo('errore!!');
        $this->showUtente($vd);
    }
    
    private function getSquadra(&$team, $subpage){
        $team = SquadreFactory::getInstance()->getSquadra($subpage);
        if(isset($team))
            return $team->getNome();
        return null;
    }
    
    private function getSquadrabyId($id){
        $team = SquadreFactory::getInstance()->getSquadrabyId($id);
        if(isset($team))
            return $team;
        return null;
    }
    
    private function getGiocatore($id){
        $player = PlayerFactory::getInstance()->getPlayerbyId($id);
        if($player)
            return $player;
        else
            return null;
    }
    
    private function getGiocatoriTeam($team_id){
        $lista = PlayerFactory::getInstance()->getPlayerbyTeamId($team_id);
        if(isset($lista))
            return $lista;
        return null;   
    }
    
    private function creaListaSquadre($utente){
        $lista = SquadreFactory::getInstance()->getlistaSquadre($utente->getUser());
        if(isset($lista))
            return $lista;
        return null;
    }
    
    private function creaListaPlayer($role){
        $lista = PlayerFactory::getInstance()->getListaPlayer($role);
        if(isset($lista))
            return $lista;
        return null;
    }
    
    private function compraGiocatore($id_p){
        $player = PlayerFactory::getInstance()->getPlayerbyId($id_p);
        return PlayerFactory::getInstance()->Compra($id_p, $_SESSION['idteam'], $player->getPrezzo(), $_SESSION['crediti']);
    }
    
    private function vendiGiocatore($id_p){
        $player = PlayerFactory::getInstance()->getPlayerbyId($id_p);
        return PlayerFactory::getInstance()->Vendi($id_p, $_SESSION['idteam'], $player->getPrezzo(), $_SESSION['crediti']);
    }
    
    //viewSquadra
    private function arrayFilter($listaTeam, $role){
        $filtro = array();
        foreach($listaTeam as $listed){
            if($listed->getRuolo() == $role)
                $filtro[] = $listed;
        }
        return $filtro;
    }
}