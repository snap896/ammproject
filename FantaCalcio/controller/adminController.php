<?php

class adminController extends defaultController{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function pageSet($request) {
        $vd = new ViewDescriptor();
        
        if($this->logged($vd)){
            if(isset($request['subpage'])){
                switch($request['subpage']){
                    case 'news':
                        $vd->setSubcontent('news');
                        break;
                    default:
                        break;
                }
            }

            if(isset($request['cmd'])){
                switch($request['cmd']){
                    case 'logout':
                        $this->logout();
                        $this->showLogin($vd);
                        break;
                }
            }
        }
        require 'view/master.php';
    }
}