<?php

include_once 'view/ViewDescriptor.php';
include_once 'model/UserFactory.php';


class defaultController{
    
    public function __construct() {}
    
    public function pageSet(&$request){
        $vd = new ViewDescriptor();
        
        if(!$this->logged($vd)){
            if(isset($request['subpage'])){
                switch($request['subpage']){
                    case 'news':
                        $vd->setSubcontent('news');
                        break;
                    case 'registrati':
                        $vd->setSubcontent('registrati');
                        break;
                    case 'info':
                        $vd->setSubcontent('info');
                        break;
                    default:
                        break;
                }
            }
            if(isset($request['cmd'])){
                switch($request['cmd']){
                    case 'login':
                        $username = isset($request['user']) ? $request['user'] : '';
                        $password = isset($request['psw']) ? $request['psw'] : '';
                        $this->login($vd, $username, $password);
                        break;
                    case 'signup':
                        if($this->filterInput($request['reg_user'], 'regexp'))
                            $reg_username = isset($request['reg_user'])? $request['reg_user'] : '';
                        else 
                            $vd->setErrorReg(' username');
                        if($this->filterInput($request['reg_mail'], 'email'))
                            $reg_mail = isset($request['reg_mail'])? $request['reg_mail'] : '';
                        else
                            $vd->setErrorReg(' email');
                        if($this->filterInput($request['reg_pass'], 'regexp'))
                            $reg_password = isset($request['reg_pass'])? $request['reg_pass'] : '';
                        else 
                            $vd->setErrorReg (' password');
                        if($vd->getErrorReg() === null){
                            $reg_img = $this->uploadImage('reg_image', $reg_username, 'userIMG');
                            if(isset($reg_img))
                                $this->signUp($vd, $reg_username, $reg_password, $reg_mail, $reg_img);
                            else
                                $this->signUp($vd, $reg_username, $reg_password, $reg_mail, NULL);
                        }
                        break;
                    default:
                        $this->showLogin($vd);
                        break;
                }
            }
        }
        require 'view/master.php';
    }
    
    protected function showUtente(ViewDescriptor $vd){
        if($_SESSION['role'])
            $this->showAdmin($vd);
        else
            $this->showUser($vd);
    }
    
    protected function showLogin(ViewDescriptor $vd){
        $vd->setSide('default/loginForm.php');
        $vd->setContent('default/staticNews.php');
        $vd->setMenu('default/menu.php');
    }
    
    private function showUser(ViewDescriptor $vd){
        $vd->setSide('utente/side.php');
        $vd->setContent('utente/userContent.php');
        $vd->setMenu('utente/menu.php');
    }
    
    private function showAdmin(ViewDescriptor $vd){
        $vd->setSide('admin/adminTab.php');
        $vd->setContent('admin/addNews.php');
        $vd->setMenu('admin/menu.php');
    }
    
    private function login($vd, $username, $password){
        $utente = UserFactory::getInstance()->autentica($username, $password);
        if(isset($utente)){
            $_SESSION['user'] = $utente->getUser();
            $_SESSION['role'] = $utente->getAdmin();
            $this->showUtente($vd);
        }
        else
            $this->showLogin($vd);
    }
    
    protected function logged(ViewDescriptor $vd){
        if(isset($_SESSION['user'])){           
            $this->showUtente($vd);
            return true;
        }
        else{
            $vd->setSubcontent('news');
            $this->showLogin($vd);
            return false;
        } 
    }
    
    protected function logout(){
        unset($_SESSION);
        if(session_id() != "" || isset($_COOKIE[session_name()]))
            setcookie(session_name(), '', time() -2592000, '/');
        session_destroy();
    }
    
    private function signUp($vd, $user, $pass, $mail, $image){
        if(!UserFactory::getInstance()->crea($user, $pass, $mail, $image))
            echo 'errore!!!';
        $this->showLogin($vd);
    }
    
    protected function getUtente(){
        $utente = UserFactory::getInstance()->cercaUtente($_SESSION['user']);
        return $utente;
    }
    
    protected function filterInput($input, $type){
        $regExp = array('options' => array('regexp' => '/[A-Za-z0-9-_]+/'));
        switch($type){
            case 'regexp':
                if($input === filter_var($input, FILTER_VALIDATE_REGEXP, $regExp))
                    return true;
                break;
            case 'email':
                if($input === filter_var($input, FILTER_VALIDATE_EMAIL))
                    return true;
                break;
            default:
                break;
        }
        return false;
    }
    
    protected function uploadImage($file, $user, $dir){
        if($_FILES[$file]['size'] == 0)
            return null;
        $path = $_SERVER['DOCUMENT_ROOT'].'/FantaCalcio/img/'.$dir.'/';
        $ext = pathinfo($_FILES[$file]['name'], PATHINFO_EXTENSION);
        $filename = $user.'.'.$ext;
        $path.= $filename;
        if(!getimagesize($_FILES[$file]['tmp_name']))
            return null;
        if($_FILES[$file]['size'] > 500000)
            return null;
        if($ext != 'gif' && $ext != 'png' && $ext != 'jpeg' && $ext != 'jpg')
            return null;
        if(!copy($_FILES[$file]['tmp_name'], $path))
            return null;
        return $filename;
    }
}