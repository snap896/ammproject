<?php

    class ViewDescriptor{
        
        private $json;
        private $subContent;
        private $content;
        private $menu;
        private $side;
        private $errorLog;
        private $errorReg;
        
        public function __construct() {}
        
        public function setJson(){
            $this->json = true;
        }
        
        public function json(){
            return $this->json;
        }
        
        public function setSubcontent($subContent){
            $this->subContent = $subContent;           
        }
        
        public function getSubcontent(){
            return $this->subContent;
        }
        
        public function setContent($content){
            $this->content = $content;
        }
        
        public function getContent(){
            return $this->content;
        }
        
        public function setMenu($menu){
            $this->menu = $menu;
        }
        
        public function getMenu(){
            return $this->menu;
        }
        
        public function setSide($side){
             $this->side = $side;
        }

        public function getSide(){
             return $this->side;
        }
        
        public function setErrorLog($errorLog){
            $this->errorLog = $errorLog;
        }
        
        public function getErrorLog(){
            return $this->errorLog;
        }
        
        public function setErrorReg($errorReg){
            $this->errorReg = $errorReg;
        }
        
        public function getErrorReg(){
            return $this->errorReg;
        }
    }