<div id="creasquadra">
    <h3>Crea la tua squadra</h3><br><br><br>
    <form method="post" action="user/squadre" enctype="multipart/form-data">
        <input type="hidden" name="cmd" value="creaSquadra">
        <div class="column">
            <label for="reg_nomeSquadra">Scegli il nome della tua squadra</label>
            <input type="text" name="reg_nomeSquadra" id="reg_nomeSquadra" pattern="[a-zA-Z]*" required/>

        </div>
        <div class="column">    
            <label for="reg_imageSquadra">Carica un immagine</label><br>
            <img src="<?= isset($_SESSION['preview']) ? '' : '' ?>" alt="" height="80" width="80"><br><br>
            <input type="file" name ="reg_imageSquadra" id="reg_imageSquadra"/>
            <br><br>
            <input type="submit" value="Crea" class="logbutton"/>
        </div>
        <br style="clear: both">
    </form>
</div>