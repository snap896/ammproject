<h3><?= $team->getNome() ?></h3>
<div class="column">
    <img src="<?= Settings::getHost().'/img/loghiUser/'.$team->getLogo() ?>" 
         alt="Immagine Mancante" height="150" width="150"/>
</div>
<div class="column">
    <p>
        Crediti: <?= $team->getCrediti() ?><br><br>
        Posizione in classifica: <br><br><br>
    </p>
    <button onclick="location.href = 'user/formazione'" type="button" id="btnFormazione"
            <?= count($listaTeam) < Settings::MAX_PLAYER ? ' disabled' : ''?>>Inserisci Formazione</button>
</div>

<table class="table_player">
    <thead>
        <tr>
        <th>Ruolo</th>
        <th>Giocatore</th>
        <th>Prezzo</th>
        <th>Action</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $filtroP = $this->arrayFilter($listaTeam, 'P');
    for($i = 0, $j = 1, $tmp = $filtroP; $i < Settings::MAX_P; $i++, $j++, $tmp = next($filtroP)){?>
            <tr class="<?= $tmp == false ? 'no_class' : 'info_player' ?>"
                id="<?= $tmp == false ? 'empty'.$j : $filtroP[$i]->getId() ?>">
            <td>P</td>
            <td><?= $tmp == false ? '' : $filtroP[$i]->getNome().' '.$filtroP[$i]->getCognome() ?></td>
            <td><?= $tmp == false ? '' : $filtroP[$i]->getPrezzo() ?></td>
            <td>
                <form method="post" action="user/mercato" class="compra_view">
                    <input type="hidden" name="role" value="P">
                    <input type="submit" value="Acquista" <?= $tmp == false ? '' : 'disabled'?>>
                </form>
                <form method="post" action="user/<?= $_SESSION['nometeam'] ?>" class="vendi_view">
                    <input type="hidden" name="cmd" value="vendi">
                    <input type="hidden" name="id_player" value="<?= $tmp == false ? '' : $filtroP[$i]->getId() ?>">
                    <input type="submit" value="Vendi" <?= $tmp == false ? 'disabled' : '' ?>>
                </form>
            </td>
            </tr>
    <?php
    }
    
    $filtroD = $this->arrayFilter($listaTeam, 'D');
    for($i = 0, $tmp = $filtroD; $i < Settings::MAX_D; $i++, $j++, $tmp = next($filtroD)){?>
            <tr class="<?= $tmp == false ? 'no_class' : 'info_player' ?>"
                id="<?= $tmp == false ? 'empty'.$j : $filtroD[$i]->getId() ?>">
            <td>D</td>
            <td><?= $tmp == false ? '' : $filtroD[$i]->getNome().' '.$filtroD[$i]->getCognome() ?></td>
            <td><?= $tmp == false ? '' : $filtroD[$i]->getPrezzo() ?></td>
            <td>
                <form method="get" action="user/mercato" class="compra_view">
                    <input type="hidden" name="role" value="D">
                    <input type="submit" value="Acquista" <?= $tmp == false ? '' : 'disabled'?>>
                </form>
                <form method="post" action="user/<?= $_SESSION['nometeam'] ?>" class="vendi_view">
                    <input type="hidden" name="cmd" value="vendi">
                    <input type="hidden" name="id_player" value="<?= $tmp == false ? '' : $filtroD[$i]->getId() ?>">
                    <input type="submit" value="Vendi" <?= $tmp == false ? 'disabled' : '' ?>>
                </form>
            </td>
            </tr>
    <?php
    }
    $filtroC = $this->arrayFilter($listaTeam, 'C');
    for($i = 0, $tmp = $filtroC; $i < Settings::MAX_C; $i++, $j++, $tmp = next($filtroC)){?>
            <tr class="<?= $tmp == false ? 'no_class' : 'info_player' ?>"
                id="<?= $tmp == false ? 'empty'.$j : $filtroC[$i]->getId() ?>">
            <td>C</td>
            <td><?= $tmp == false ? '' : $filtroC[$i]->getNome().' '.$filtroC[$i]->getCognome() ?></td>
            <td><?= $tmp == false ? '' : $filtroC[$i]->getPrezzo() ?></td>
            <td>
                <form method="get" action="user/mercato" class="compra_view">
                    <input type="hidden" name="role" value="C">
                    <input type="submit" value="Acquista" <?= $tmp == false ? '' : 'disabled'?>>
                </form>
                <form method="post" action="user/<?= $_SESSION['nometeam'] ?>" class="vendi_view">
                    <input type="hidden" name="cmd" value="vendi">
                    <input type="hidden" name="id_player" value="<?= $tmp == false ? '' : $filtroC[$i]->getId() ?>">
                    <input type="submit" value="Vendi" <?= $tmp == false ? 'disabled' : '' ?>>
                </form>
            </td>
            </tr>
    <?php
    }
    $filtroA = $this->arrayFilter($listaTeam, 'A');
    for($i = 0, $tmp = $filtroA; $i < Settings::MAX_A; $i++, $j++, $tmp = next($filtroA)){?>
        <tr class="<?= $tmp == false ? 'no_class' : 'info_player' ?>"
            id="<?= $tmp == false ? 'empty'.$j : $filtroA[$i]->getId() ?>">
            <td>A</td>
            <td><?= $tmp == false ? '' : $filtroA[$i]->getNome().' '.$filtroA[$i]->getCognome() ?></td>
            <td><?= $tmp == false ? '' : $filtroA[$i]->getPrezzo() ?></td>
            <td>
                <form method="get" action="user/mercato" class="compra_view">
                    <input type="hidden" name="role" value="A">
                    <input type="submit" value="Acquista"<?= $tmp == false ? '' : 'disabled'?>>
                </form>
                <form method="post" action="user/<?= $_SESSION['nometeam'] ?>" class="vendi_view">
                    <input type="hidden" name="cmd" value="vendi">
                    <input type="hidden" name="id_player" value="<?= $tmp == false ? '' : $filtroA[$i]->getId() ?>">
                    <input type="submit" value="Vendi" <?= $tmp == false ? 'disabled' : '' ?>>
                </form>
            </td>
        </tr>
    <?php
    }?>
    </tbody>
</table>