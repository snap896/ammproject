<table class="table_player">
    <thead>
        <tr>
            <th>Nome</th>
            <th>Cognome</th>
            <th>Ruolo</th>
            <th>Prezzo</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($listaPlayer as $listed){
        ?>
            <tr class="info_player" id="<?= $listed->getId() ?>">
                <td><?= $listed->getNome() ?></td>
                <td><?= $listed->getCognome() ?></td>
                <td><?= $listed->getRuolo() ?></td>
                <td><?= $listed->getPrezzo() ?></td>
            </tr>
        <?php
        }?>
    </tbody>
</table>