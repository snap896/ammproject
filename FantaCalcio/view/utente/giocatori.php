<?php
    $json = array();
    $json['nome'] = $player->getNome();
    $json['cognome'] = $player->getCognome();
    $json['ruolo'] = $player->getRuolo();
    $json['squadra'] = $player->getSquadra();
    $json['naz'] = $player->getNaz();
    $json['pres'] = $player->getPres();
    $json['gol'] = $player->getGol();
    $json['prezzo'] = $player->getPrezzo();
    $json['foto'] = $player->getFoto();
    $json['url'] = Settings::getHost();
    echo json_encode($json);