<h3>Acquista giocatori</h3><h2><?= $_SESSION['nometeam'] ?></h2>
Crediti: <?= $_SESSION['crediti'] ?>
<table class="table_player">
    <thead>
        <th>Nome</th>
        <th>Cognome</th>
        <th>Ruolo</th>
        <th>Prezzo</th>
        <th>Azioni</th>
    </thead>
    <tbody>
        <?php
        foreach($listaCompra as $listed){
        ?>
            <tr class="info_player" id="<?= $listed->getId() ?>">
                <td><?= $listed->getNome() ?></td>
                <td><?= $listed->getCognome() ?></td>
                <td><?= $listed->getRuolo() ?></td>
                <td><?= $listed->getPrezzo() ?></td>
                <td>
                    <form method="post" action="user/<?= $_SESSION['nometeam']?>" id="compra_mercato">
                        <input type="hidden" name="cmd" value="compra">
                        <input type="hidden" name="player" value="<?= $listed->getId() ?>">
                        <input type="hidden" name="role" value="<?= $listed->getRuolo() ?>">
                        <input type="submit" value="Compra">
                    </form>
                </td>
            </tr>
        <?php
        }?>
    </tbody>
</table>