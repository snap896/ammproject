<h2>Formazione</h2><br><br>
<p>Scegli il modulo e inserisci la tua formazione trascinando i giocatori nel
    loro ruolo.</p><br><br><br>
<div id="campo_calcio">
    <p>
    <select id="modulo">
        <option value="Scegli" selected disabled hidden>Scegli</option>
        <option value="3-4-3">3-4-3</option>
        <option value="3-5-2">3-5-2</option>
        <option value="4-4-2">4-4-2</option>
        <option value="4-3-3">4-3-3</option>
        <option value="4-5-1">4-5-1</option>
        <option value="5-3-2">5-3-2</option>
        <option value="5-4-1">5-4-1</option>
    </select>
    </p>
    <div id="portiere" class="titolari" ondrop="drop(event)" ondragover="dragOver(event)"
         ondragenter="dragEnter(event)"></div>
    <ul id="formazione">
        <li id="difesa" class="strips"></li>
        <li id="centrocampo" class="strips"></li>
        <li id="attacco" class="strips"></li>
    </ul>
</div>
<div id="ele_container">
    <?php
    for($i = 0; $i < 25; $i++){?>
        <div id="<?=$lista[$i]->getId()?>" class="ele_team info_player" draggable="true" ondragstart="dragStart(event)">
            <img src="<?= Settings::getHost().'/img/fotoPlayer/'.$lista[$i]->getFoto() ?>"
                 alt="" height="60" width="50">
        </div>
    <?php
    }?>
</div>