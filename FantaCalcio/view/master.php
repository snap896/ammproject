<?php
if(!$vd->json()){
?>
    <!DOCTYPE html>
    <!--Pagina web sul fantacalcio - amm2015-->
    <html>
        <head>
            <title>Fantacampionato!!</title>
            <base href=<?= Settings::getHost()?>/>
            <meta http-equiv="content-type" content="text/html; charset=UTF-8">
            <meta name="keywords" content="fantacalcio seriea calcio">
            <meta name="description" content="Gioca al fantacalcio!">
            <link rel="stylesheet" type="text/css" href="css/css_fanta.css" media="screen">
            <script src="javascript/jquery-1.11.3.js"></script>
            <script src="javascript/views.js"></script>
            <script src="javascript/buttons.js"></script>
            <script src="javascript/formazione.js"></script>
        </head>
        <body>
            <div id="page">
                <!--header-->
                <header>
                    <!--social buttons-->
                    <div id="social_share">
                        <p>Share us:</p>
                        <ul>
                            <li id="mail">
                                <a  href="mailto:?subject=Fantacalcio&Body=Gioca%20con%20me%20al%20fantacalcio!%20 http://spano.sc.unica.it/amm2014/floreManuele/index.html" 
                                    rel="mail"><img src="<?= Settings::getHost().'/img/Email.png' ?>" alt='mail'></a>
                            </li>
                            <li id="facebook">
                                <a href="http://www.facebook.com/sharer.php?u=http://spano.sc.unica.it/amm2014/floreManuele/index.html" 
                                   rel="facebook" target="_blank"><img src="<?= Settings::getHost().'/img/facebook.png' ?>" alt='fb'></a>
                            </li>
                            <li id="twitter">
                                <a  href="http://twitter.com/share?url=http://spano.sc.unica.it/amm2014/floreManuele/index.html" 
                                    rel="twitter" target="_blank"><img src="<?= Settings::getHost().'/img/twitter.png' ?>" alt='tweet'></a>
                            </li>
                            <li id="googleplus">
                                <a  href="https://plus.google.com/share?url=http://spano.sc.unica.it/amm2014/floreManuele/index.html" 
                                    rel="google" target="_blank"><img src="<?= Settings::getHost().'/img/google-plus.png' ?>" alt='g+'></a>
                            </li>
                        </ul>
                    </div>
                    <div id="header">
                        <div id="logo">
                            <h1>FC</h1>
                        </div>
                    </div>
                </header>
                <!--page start-->
                <div id="main_container">
                    <!--navigation pane-->
                    <div id="navigation">
                        <div id="menu" class="scrollable">
                        <?php
                        $menu = $vd->getMenu();
                        include "$menu";
                        ?>
                        </div>
                    </div>
                    <!--news/page content/user content-->
                    <div id="content">
                        <?php
                        $content = $vd->getContent();
                        include "$content";
                        ?>
                    </div>
                    <!--login pane/user pane-->
                    <div id="side2">
                        <?php
                        $side = $vd->getSide();
                        include "$side";
                        ?>
                    </div>
                </div>
                <!--clear-->
                <div id="clear"></div>
                <!--footer-->
                <footer>
                </footer>
            </div>
        </body>
    </html>
<?php
}
else{
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
}