<div id="info">
<h1>Progetto AMM FantaCalcio</h1><br><br>
<p>
    L'applicazione gestisce un semplice sito sul gioco del fantacalcio.<br> 
    L'homepage si trova al seguente <a 
        href="http://spano.sc.unica.it/amm2014/floreManuele/FantaCalcio/index.php?page=home">link</a>.
</p><br>
<p>
    Per poter giocare &egrave; necessario possedere un account utente. &Egrave;
    possibile creare un account scegliendo l'opzione "Registrati" 
    presente nel men&ugrave; nella sinistra dell'homepage.
</p><br>
<p>  
    Per facilitare l'accesso &egrave; presente un account utente gi&aacute; pronto:
</p><br>
    username: Davide<br>
    password: amm-Fanta<br><br>
<p>
    L'applicativo gestisce due tipi di account, l'account utente (account base creato tramite il menu "Registrati")
    e l'account amministratore che gestisce il funzionamento del gioco e ne permette lo svolgimento regolare (inserimento
    e calcolo punteggi, scelta prezzo giocatori, etc)
</p>
<p>
    Per accedere con un account amministratore bisogna effettuare il login con le seguenti credenziali:
</p><br>
    username: Manuele<br>
    password: amm-FantaAdmin<br><br>
<p>
    Il progetto &egrave; sviluppato utilizzando: <br><br>
</p>
<ul>
    <li>PHP, HTML, Javascript, MySql e il pattern MVC.</li>
    <li>Un database per la memorizzazione dei dati.</li>
    <li>Le transazioni di mysqli (FantaCalcio/model/PlayerFactory.php:141)</li>
    <li>jQuery e le chiamate asincrone ajax (FantaCalcio/javascript/views.js:137)</li>
</ul><br><br>
<h4>Schema E/R del database</h4>
<br>
<img src="<?= Settings::getHost().'/img/FantaCalcio-amm.png'?>" alt="figura db" height="100%" width="100%"> <br><br>
<p>
    Alcune funzionalit&aacute; del lato utente non sono implementate (probabili formazioni, classifica)
    e altre presentano dei bug (la registrazione non da dei feedback, se non per il formato dell'input;
    idem per la creazione di una squadra) entrambe queste viste danno la possibilit&aacute; di caricare un immagine dell'utente,
    ma perch&eacute; funzioni bisogna cambiare il gruppo della cartella in cui le immagini sono salvate (da' errore solo se si
    prova a caricare un immagine).
</p><br>
<p>
    Un altra funzionalit&aacute; parziale &egrave; quella delle formazioni. La funzionalit&aacute; drag and drop &egrave;
    implementata solo parzialmente.<br>
    Il lato amministratore invece &egrave; completamente vuoto.
</p>
</div>