<div id="register">
    <h3>Registrazione</h3><br><br><br>
    <form method="post" action="home/registrati" enctype="multipart/form-data">
        <input type="hidden" name="cmd" value="signup">
        <div class="column">
            <label for="reg_user">Scegli il tuo username</label>
            <input type="text" name="reg_user" id="reg_user" required/>
            <br>
            <label for="reg_mail">Inserisci la tua mail</label>
            <input type="email" name="reg_mail" id="reg_mail" required/>
            <br>
            <label for="reg_pass">Scegli una password</label>
            <input type="password" name="reg_pass" id="reg_pass" required/>
            <br><br>
            <input type="submit" value="Registrati" class="logbutton"/>
        </div>
        <div class="column">  
            <label for="reg_image">Carica un immagine</label><br>
            <img src="" alt="" height="80" width="80"><br><br>
            <input type="file" name ="reg_image" id="reg_image"/>       
        </div>
    </form>
    <span id="errorReg"><?=  $vd->getErrorReg() !== null ? 
       'Errore nella registrazione:'.$vd->getErrorReg().' non conforme!' : ''?></span>
</div>
