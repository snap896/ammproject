-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Set 15, 2015 alle 17:36
-- Versione del server: 5.5.41-0ubuntu0.14.04.1
-- Versione PHP: 5.5.9-1ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `FantaCalcio-amm`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `Formazione`
--

CREATE TABLE IF NOT EXISTS `Formazione` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `modulo` varchar(5) DEFAULT NULL,
  `idGiocatori` varchar(68) DEFAULT NULL,
  `schiera_idsquadre` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `schiera_idsquadre` (`schiera_idsquadre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `giocatori_squadre`
--

CREATE TABLE IF NOT EXISTS `giocatori_squadre` (
  `giocatori_id` bigint(20) unsigned NOT NULL,
  `squadre_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`giocatori_id`,`squadre_id`),
  KEY `squadre_id` (`squadre_id`),
  KEY `giocatori_id` (`giocatori_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `giocatori_squadre`
--

INSERT INTO `giocatori_squadre` (`giocatori_id`, `squadre_id`) VALUES
(2, 1),
(3, 1),
(4, 1),
(8, 1),
(10, 1),
(21, 1),
(49, 1),
(1, 4),
(2, 9),
(3, 9),
(4, 9),
(6, 9),
(7, 9),
(12, 9),
(14, 9),
(15, 9),
(16, 9),
(17, 9),
(20, 9),
(21, 9),
(22, 9),
(23, 9),
(27, 9),
(28, 9),
(33, 9),
(34, 9),
(35, 9),
(39, 9),
(43, 9),
(49, 9),
(50, 9),
(1, 10),
(3, 10),
(5, 10),
(6, 10),
(9, 10),
(10, 10),
(12, 10),
(14, 10),
(15, 10),
(16, 10),
(17, 10),
(21, 10),
(22, 10),
(23, 10),
(26, 10),
(27, 10),
(29, 10),
(30, 10),
(32, 10),
(38, 10),
(39, 10),
(41, 10),
(42, 10),
(44, 10),
(47, 10);

-- --------------------------------------------------------

--
-- Struttura della tabella `ListaGiocatori`
--

CREATE TABLE IF NOT EXISTS `ListaGiocatori` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(128) DEFAULT NULL,
  `cognome` varchar(128) DEFAULT NULL,
  `squadra` varchar(128) DEFAULT NULL,
  `ruolo` char(1) DEFAULT NULL,
  `naz` varchar(128) DEFAULT NULL,
  `presenze` smallint(5) unsigned DEFAULT NULL,
  `gol` smallint(5) DEFAULT NULL,
  `prezzo` smallint(5) unsigned DEFAULT NULL,
  `foto` varchar(128) NOT NULL DEFAULT 'default-avatar.png',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dump dei dati per la tabella `ListaGiocatori`
--

INSERT INTO `ListaGiocatori` (`id`, `nome`, `cognome`, `squadra`, `ruolo`, `naz`, `presenze`, `gol`, `prezzo`, `foto`) VALUES
(1, 'Gianluigi', 'Buffon', 'logo_juve.png', 'P', 'logo_italia.png', 555, -465, 17, 'default-avatar.png'),
(2, 'Samir', 'Handanovic', 'logo_inter.png', 'P', 'logo_slovenia.png', 294, -361, 14, 'default-avatar.png'),
(3, 'Morgan', 'De Sanctis', 'logo_roma.png', 'P', 'logo_italia.png', 415, -444, 13, 'default-avatar.png'),
(4, 'Diego', 'Lopez Rodriguez', 'logo_milan.png', 'P', 'logo_spagna.png', 28, -42, 11, 'default-avatar.png'),
(5, 'Federico', 'Marchetti', 'logo_lazio.png', 'P', 'logo_italia.png', 183, -216, 15, 'default-avatar.png'),
(6, 'Giorgio', 'Chiellini', 'logo_juve.png', 'D', 'logo_italia.png', 292, 22, 12, 'default-avatar.png'),
(7, 'Leonardo', 'Bonucci', 'logo_juve.png', 'D', 'logo_italia.png', 201, 10, 13, 'default-avatar.png'),
(8, 'Andrea', 'Ranocchia', 'logo_inter.png', 'D', 'logo_italia.png', 152, 9, 8, 'default-avatar.png'),
(9, 'Danilo', 'D''Ambrosio', 'logo_inter.png', 'D', 'logo_italia.png', 76, 4, 6, 'default-avatar.png'),
(10, 'Philippe', 'Mexes', 'logo_milan.png', 'D', 'logo_francia.png', 264, 16, 8, 'default-avatar.png'),
(11, 'Christian', 'Zapata', 'logo_milan.png', 'D', 'logo_colombia.png', 223, 6, 5, 'default-avatar.png'),
(12, 'Kostas', 'Manolas', 'logo_roma.png', 'D', 'logo_grecia.png', 30, 0, 11, 'default-avatar.png'),
(14, 'Kamil', 'Glik', 'logo_torino.png', 'D', 'logo_polonia.png', 114, 10, 16, 'default-avatar.png'),
(15, 'Nikola', 'Maksimovic', 'logo_torino.png', 'D', 'logo_serbia.png', 50, 0, 5, 'default-avatar.png'),
(16, 'Stefan', 'De Vrij', 'logo_lazio.png', 'D', 'logo_olanda.png', 30, 0, 9, 'default-avatar.png'),
(17, 'Senad', 'Lulic', 'logo_lazio.png', 'D', 'logo_bosnia.png', 115, 15, 15, 'default-avatar.png'),
(19, 'Manuel', 'Pasqual', 'logo_fiorentina.png', 'D', 'logo_italia.png', 285, 7, 11, 'default-avatar.png'),
(20, 'Javier', 'Gonzalo Rodriguez', 'logo_fiorentina.png', 'D', 'logo_argentina.png', 98, 17, 17, 'default-avatar.png'),
(21, 'Marek', 'Hamsik', 'logo_napoli.png', 'C', 'logo_slovacchia.png', 281, 75, 21, 'default-avatar.png'),
(22, 'Dries', 'Mertens', 'logo_napoli.png', 'C', 'logo_belgio.png', 64, 17, 22, 'default-avatar.png'),
(23, 'Paul', 'Pogba', 'logo_juve.png', 'C', 'logo_francia.png', 89, 20, 25, 'default-avatar.png'),
(25, 'Nigel', 'De Jong', 'logo_milan.png', 'C', 'logo_olanda.png', 74, 6, 11, 'default-avatar.png'),
(26, 'Riccardo', 'Montolivo', 'logo_milan.png', 'C', 'logo_italia.png', 322, 27, 10, 'default-avatar.png'),
(27, 'Gary', 'Medel', 'logo_inter.png', 'C', 'logo_cile.png', 35, 0, 8, 'default-avatar.png'),
(28, 'Fredy', 'Guarin', 'logo_inter.png', 'C', 'logo_colombia.png', 98, 14, 13, 'default-avatar.png'),
(29, 'Miralem', 'Pjanic', 'logo_roma.png', 'C', 'logo_bosnia.png', 125, 17, 20, 'default-avatar.png'),
(30, 'Daniele', 'De Rossi', 'logo_roma.png', 'C', 'logo_italia.png', 364, 36, 15, 'default-avatar.png'),
(31, 'Pereyra Gomes', 'Felipe Anderson', 'logo_lazio.png', 'C', 'logo_brasile.png', 45, 10, 22, 'default-avatar.png'),
(32, 'Lucas', 'Biglia', 'logo_lazio.png', 'C', 'logo_argentina.png', 53, 5, 13, 'default-avatar.png'),
(33, 'David', 'Pizarro', 'logo_fiorentina.png', 'C', 'logo_cile.png', 381, 28, 9, 'default-avatar.png'),
(34, 'Iglesias', 'Borja Valero', 'logo_fiorentina.png', 'C', 'logo_spagna.png', 97, 9, 10, 'default-avatar.png'),
(35, 'Claudio', 'Marchisio', 'logo_juve.png', 'C', 'logo_italia.png', 239, 32, 15, 'default-avatar.png'),
(36, 'Alvaro', 'Morata', 'logo_juve.png', 'A', 'logo_spagna.png', 29, 8, 26, 'default-avatar.png'),
(37, 'Luca', 'Toni', 'logo_hellas.png', 'A', 'logo_italia.png', 321, 151, 30, 'default-avatar.png'),
(38, 'Antonio', 'Di Natale', 'logo_udinese.png', 'A', 'logo_italia.png', 422, 207, 29, 'default-avatar.png'),
(39, 'Gonzalo', 'Higuain', 'logo_napoli.png', 'A', 'logo_argentina.png', 69, 35, 31, 'default-avatar.png'),
(40, 'Mauro', 'Icardi', 'logo_inter.png', 'A', 'logo_argentina.png', 89, 41, 30, 'default-avatar.png'),
(41, 'Francesco', 'Totti', 'logo_roma.png', 'A', 'logo_italia.png', 588, 243, 24, 'default-avatar.png'),
(42, 'Miroslav', 'Klose', 'logo_lazio.png', 'A', 'logo_germania.png', 115, 47, 20, 'default-avatar.png'),
(43, 'Manolo', 'Gabbiadini', 'logo_napoli.png', 'A', 'logo_italia.png', 122, 24, 18, 'default-avatar.png'),
(44, 'Jeremy', 'Menez', 'logo_milan.png', 'A', 'logo_francia.png', 117, 23, 20, 'default-avatar.png'),
(45, 'Domenico', 'Berardi', 'logo_sassuolo.png', 'A', 'logo_italia.png', 61, 31, 21, 'default-avatar.png'),
(47, 'German', 'Denis', 'logo_atalanta.png', 'A', 'logo_argentina.png', 226, 69, 19, 'default-avatar.png'),
(48, 'Rodrigo', 'Palacio', 'logo_inter.png', 'A', 'logo_argentina.png', 188, 72, 19, 'default-avatar.png'),
(49, 'Giampaolo', 'Pazzini', 'logo_milan.png', 'A', 'logo_italia.png', 307, 101, 16, 'default-avatar.png'),
(50, 'Andrea', 'Belotti', 'logo_palermo.png', 'A', 'logo_italia.png', 38, 6, 10, 'default-avatar.png');

-- --------------------------------------------------------

--
-- Struttura della tabella `Squadre`
--

CREATE TABLE IF NOT EXISTS `Squadre` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(128) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `crediti` int(10) unsigned NOT NULL DEFAULT '350',
  `logo` varchar(128) DEFAULT NULL,
  `possiede_username` varchar(128) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `nome` (`nome`),
  KEY `possiede_username` (`possiede_username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dump dei dati per la tabella `Squadre`
--

INSERT INTO `Squadre` (`id`, `nome`, `crediti`, `logo`, `possiede_username`) VALUES
(1, 'Longobarda', 212, 'longobarda.jpg', 'utente1'),
(4, 'new-team', 333, 'new-team.jpg', 'utente1'),
(9, 'parisSantambre', 16, 'parisSantambre.png', 'utente1'),
(10, 'borgorosso', 96, 'borgorosso.png', 'mimi');

-- --------------------------------------------------------

--
-- Struttura della tabella `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `username` varchar(128) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `password` varchar(128) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `mail` varchar(128) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `punteggio` smallint(5) unsigned NOT NULL DEFAULT '0',
  `admin` tinyint(1) DEFAULT NULL,
  `imgSource` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `mail` (`mail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `Users`
--

INSERT INTO `Users` (`username`, `password`, `mail`, `punteggio`, `admin`, `imgSource`) VALUES
('ciccio', 'ciccietto', 'ciccio@mail.com', 0, 0, 'ciccio.png'),
('mimi', 'mimirocks', 'mimi@gmail.com', 0, 0, 'mimi.png'),
('snap', 'fantacalcio', 'flore.manuele@gmail.com', 0, 1, 'left41.gif'),
('utente1', 'tiptap', 'tiptap@mail.it', 0, 0, 'rat.jpeg');

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `Formazione`
--
ALTER TABLE `Formazione`
  ADD CONSTRAINT `fk_idsquadre` FOREIGN KEY (`schiera_idsquadre`) REFERENCES `Squadre` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `giocatori_squadre`
--
ALTER TABLE `giocatori_squadre`
  ADD CONSTRAINT `ListaGiocatori_fk` FOREIGN KEY (`giocatori_id`) REFERENCES `ListaGiocatori` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `Squadre_fk` FOREIGN KEY (`squadre_id`) REFERENCES `Squadre` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Limiti per la tabella `Squadre`
--
ALTER TABLE `Squadre`
  ADD CONSTRAINT `fk_username` FOREIGN KEY (`possiede_username`) REFERENCES `Users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
