<?php

/**
 * classe che contiene i parametri per stabilire una connessione sul database
 * e che restituisce il path corretto dell'applicazione a seconda del server su cui si trova (locale, spano.sc)
 * @author Manuele Flore
 */

class Settings {
    
    const DB_HOST = "localhost";
    const DB_USER = "floreManuele";
    const DB_PASS = "gorilla915";
    const DB_NAME = "amm14_floreManuele";
    const MAX_P = 3;
    const MAX_D = 8;
    const MAX_C = 8;
    const MAX_A = 6;
    const MAX_PLAYER = 25;
    
    public static function getHost(){
        switch($_SERVER['HTTP_HOST']){
            case 'localhost':
                $path = 'http://'.$_SERVER['HTTP_HOST'].'/FantaCalcio';
                break;
            case 'spano.sc.unica.it':
                $path = 'http://'.$_SERVER['HTTP_HOST'].'/amm2014/floreManuele/FantaCalcio';
                break;
            default:
                $path = '';
        }
        return $path;
    }
}
